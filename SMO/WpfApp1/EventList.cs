﻿using System;
using System.Collections.Generic;

namespace WpfApp1
{
    public class EventList<T> : List<T>
    {
        public EventHandler<int> OnCountChanged;

        public void AddWithEvent(T item)
        {
            Add(item);
            OnCountChanged?.Invoke(this, -1);
        }

        public void AddWithEvent(T item, int requestCount)
        {
            Add(item);
            OnCountChanged?.Invoke(this, requestCount);
        }

        public void RemoveWithEvent(T item)
        {
            Remove(item);
            OnCountChanged?.Invoke(this, -1);
        }
    }
}

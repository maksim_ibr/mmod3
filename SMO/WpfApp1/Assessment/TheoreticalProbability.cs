﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Assessment
{
    public static class TheoreticalProbability
    {
        public static Tuple<List<double>, List<double>> CalculateFinalProbabilities(int requestIntensity, int channelIntensity,
            int channelsCount, int queueCapacity, double averageTime)
        {
            var finalProbabilitiesP = new List<double>();

            var leftPartP0 = 1.0;
            var intensity = (double)requestIntensity / channelIntensity;
            for (var i = 1; i <= channelsCount; i++)
            {
                leftPartP0 += Math.Pow(intensity, i) / FactorialOf(i);
            }

            var rightPartP0 = Math.Pow((double) requestIntensity / channelIntensity, channelsCount) /
                              FactorialOf(channelsCount);

            var v = 1.0 / averageTime;
            var b = v / channelIntensity;
            var tmp = 0.0;
            for (var i = 1; i <= queueCapacity; i++)
            {
                var denominatorTmp = CalculateDenominator(channelsCount, b, i);
                tmp += Math.Pow(intensity, i) / denominatorTmp;
            }

            rightPartP0 *= tmp;
            var p0 = Math.Pow(leftPartP0 + rightPartP0, -1);
            finalProbabilitiesP.Add(p0);

            var pn = 0.0;
            for (var i = 1; i <= channelsCount; i++)
            {
                var currentP = Math.Pow(intensity, i) / FactorialOf(i) * p0;
                finalProbabilitiesP.Add(currentP);

                if (i == channelsCount)
                {
                    pn = currentP;
                }
            }

            var finalProbabilitiesM = new List<double>();
            for (var i = 1; i <= queueCapacity; i++)
            {
                var currentPm = pn * Math.Pow(intensity, i) / CalculateDenominator(channelsCount, b, i);
                finalProbabilitiesM.Add(currentPm);
            }

            return new Tuple<List<double>, List<double>>(finalProbabilitiesP, finalProbabilitiesM);
        }

        private static double CalculateDenominator(int channelsCount, double b, int num)
        {
            var denominatorTmp = 1.0;
            for (var i = 1; i <= num; i++)
            {
                denominatorTmp *= (channelsCount + i * b);
            }

            return denominatorTmp;
        }

        private static int FactorialOf(int i)
        {
            if (i <= 1)
            {
                return 1;
            }

            return i * FactorialOf(i - 1);
        }
    }
}

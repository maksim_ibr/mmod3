﻿using System.Collections.Generic;
using System.Linq;

namespace WpfApp1.Assessment
{
    public static class SmoProbability
    {
        public static List<double> CalculateFinalProbabilities(List<int> stateList)
        {
            var sum = stateList.Sum();
            var result = new List<double>();
            foreach (var state in stateList)
            {
                result.Add((double)state / sum);
            }

            return result;
        }
    }
}

﻿using System.Collections.Generic;

namespace WpfApp1.Assessment
{
    public static class Assessments
    {
        public static double CalculateAbsoluteThroughput(int requestIntensity, double refusedP) =>
            requestIntensity * CalculateRelativeThroughput(refusedP);

        public static double CalculateRelativeThroughput(double refusedP) => 1 - refusedP;

        public static double CalculateAverageRequestCountInQueue(IReadOnlyList<double> finalProbabilitiesM)
        {
            var average = 0.0;
            for (var i = 1; i <= finalProbabilitiesM.Count; i++)
            {
                average += finalProbabilitiesM[i - 1] * i;
            }

            return average;
        }

        public static double CalculateAverageBusyChannels(double absoluteThroughput, int channelIntensity) =>
            absoluteThroughput / channelIntensity;

        public static double CalculateAverageRequestCountInSmo(IReadOnlyList<double> finalProbabilitiesP,
            IReadOnlyList<double> finalProbabilitiesM)
        {
            var average = 0.0;
            for (var i = 1; i < finalProbabilitiesP.Count; i++)
            {
                average += finalProbabilitiesP[i] * i;
            }

            var channelsCount = finalProbabilitiesP.Count - 1;
            for (var i = 1; i < finalProbabilitiesM.Count; i++)
            {
                average += channelsCount * finalProbabilitiesM[i - 1];
            }

            return average;
        }

        public static double CalculateAverageTimeInSmo(double refusedP, double channelIntensity, double averageTime) =>
            (1 - refusedP) / channelIntensity + averageTime;

        public static double CalculateAverageTimeInQueue(double averageTimeInSmo, double averageTime) =>
            averageTimeInSmo - averageTime;
    }
}

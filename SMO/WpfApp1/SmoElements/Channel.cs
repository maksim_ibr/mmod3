﻿using System;
using System.Threading.Tasks;

namespace WpfApp1.SmoElements
{
    public class Channel
    {
        private readonly int _requestId;

        public bool IsBusy { get; set; }
        public EventHandler<Request> OnCompletion;

        private Request _request;
        private readonly int _time;

        public Channel(int number, int time)
        {
            _requestId = number;
            _time = time;
        }

        public void StartTask(Request request)
        {
            IsBusy = true;
            _request = request;

            request.GetService(this);
            Task.Delay(_time).ContinueWith(t => FinishTask());
        }

        private void FinishTask()
        {
            IsBusy = false;
            OnCompletion?.Invoke(this, _request);
        }

        public override string ToString() => _requestId.ToString();
    }
}

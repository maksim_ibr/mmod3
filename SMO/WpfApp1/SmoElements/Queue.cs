﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfApp1.SmoElements
{
    public class Queue
    {
        private static readonly object BusyChannelsLock = new object();
        private static readonly object RequestLock = new object();

        private readonly int _queueCapacity;

        private readonly List<Channel> _channels;
        private readonly List<Request> _requests;

        public Statistics Statistics { get; }

        public Queue(Statistics statistics, IEnumerable<Channel> channels, int queueCapacity)
        {
            _queueCapacity = queueCapacity;
            _channels = channels.ToList();
            _requests = new List<Request>();
            Statistics = statistics;

            foreach (var channel in _channels)
            {
                channel.OnCompletion += OnChannelTaskCompletion;
            }
        }

        public void AddRequest(Request request)
        {
            request.OnLeave += OnRequestLeave;
            request.OnGotWaiver += OnRequestGotWaiver;

            lock (BusyChannelsLock)
            {
                lock (RequestLock)
                {
                    Statistics.AllRequestsCollection.Add(request);
                    var freeChannel = _channels.FirstOrDefault(c => !Statistics.BusyChannels.Contains(c));
                    if (freeChannel == null)
                    {
                        lock (RequestLock)
                        {
                            if (_requests.Count < _queueCapacity)
                            {
                                _requests.Add(request);
                                Statistics.Notifications.AddWithEvent(
                                    $"Запрос '{request}' становится в очередь. Длина очереди: {_requests.Count}",
                                    _requests.Count);
                            }
                            else
                            {
                                request.GetWaiver();
                            }
                        }
                    }
                    else
                    {
                        freeChannel.StartTask(request);
                        Statistics.BusyChannels.AddWithEvent(freeChannel);
                        Statistics.Notifications.AddWithEvent(
                            $"Запрос '{request}' обслуживается без очереди каналом '{freeChannel}'. Длина очереди: {_requests.Count}",
                            _requests.Count);
                    }
                }
            }
        }

        private void OnChannelTaskCompletion(object sender, Request oldRequest)
        {
            lock (BusyChannelsLock)
            {
                var channel = (Channel)sender;
                Statistics.BusyChannels.RemoveWithEvent(channel);
                Statistics.DoneRequestCollection.Add(oldRequest);

                lock (RequestLock)
                {
                    Statistics.Notifications.AddWithEvent(
                        $"Запрос {oldRequest} был обслужан каналом '{channel}'", _requests.Count);
                    if (!_requests.Any())
                    {
                        return;
                    }

                    var newRequest = _requests.First();
                    _requests.Remove(newRequest);
                    channel.StartTask(newRequest);
                    Statistics.BusyChannels.AddWithEvent(channel);
                    Statistics.Notifications.AddWithEvent(
                        $"Запрос {newRequest} начинает обслуживаться каналом '{channel}'. Длина очереди: {_requests.Count}",
                        _requests.Count);
                }
            }
        }

        private void OnRequestLeave(object sender, EventArgs args)
        {
            lock (RequestLock)
            {
                var request = (Request)sender;
                Statistics.LeaveRequestCollection.Add((Request)sender);
                _requests.Remove(request);
                Statistics.Notifications.AddWithEvent(
                    $"Запрос '{request}' покинул очередь. Длина очереди: {_requests.Count}", _requests.Count);
            }
        }

        private void OnRequestGotWaiver(object sender, EventArgs args)
        {
            lock (RequestLock)
            {
                var request = (Request)sender;
                Statistics.WaiverRequestCollection.Add((Request)sender);
                Statistics.Notifications.AddWithEvent(
                    $"Запрос '{request}' не смог стать в очередь. Длина очереди: {_requests.Count}", _requests.Count);
            }
        }
    }
}

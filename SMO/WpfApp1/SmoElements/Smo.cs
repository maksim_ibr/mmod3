﻿using System;
using System.Collections.Generic;
using System.Timers;
using MathNet.Numerics.Distributions;

namespace WpfApp1.SmoElements
{
    public class Smo
    {
        private readonly int _channelTime;
        private readonly int _requestTime;
        private readonly int _millisecondPeriod;
        private readonly double _exponentialParameter;

        private static Timer _timer;

        private readonly Queue _queue;
        private readonly Statistics _statistics;

        public Smo(Statistics statistics, int requestIntensity, int channelIntensity, double exponentialParameter,
            int channelCount, int queueCapacity, int millisecondPeriod = 1000)
        {
            _millisecondPeriod = millisecondPeriod;
            _requestTime = _millisecondPeriod / requestIntensity;
            _channelTime = _millisecondPeriod / channelIntensity;
            _exponentialParameter = exponentialParameter;
            _statistics = statistics;

            _queue = new Queue(statistics, CreateChannels(channelCount), queueCapacity);
        }

        public void Start()
        {
            _timer = new Timer { Interval = _requestTime, Enabled = true };
            _timer.Elapsed += OnNewRequest;
        }

        public List<int> Stop()
        {
            _timer.Elapsed -= OnNewRequest;

            return _statistics.StateList;
        }

        private IEnumerable<Channel> CreateChannels(int channelCount)
        {
            var channels = new List<Channel>();
            for (var i = 0; i < channelCount; i++)
            {
                channels.Add(new Channel(i + 1, _channelTime));
            }

            return channels;
        }

        private void OnNewRequest(object sender, EventArgs args) => _queue.AddRequest(
            new Request((int)(Exponential.Sample(_exponentialParameter) * _millisecondPeriod)));
    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace WpfApp1.SmoElements
{
    public class Request
    {
        private readonly CancellationToken _token;
        private readonly CancellationTokenSource _cancellToken;

        public EventHandler OnLeave;
        public EventHandler OnGotWaiver;

        private readonly Guid _requestId;

        public Request(int time)
        {
            _requestId = Guid.NewGuid();
            _cancellToken = new CancellationTokenSource();
            _token = _cancellToken.Token;
            Task.Delay(time).ContinueWith(t => Leave(), _token);
        }

        public void GetService(Channel channel) => _cancellToken.Cancel();

        public void GetWaiver()
        {
            _cancellToken.Cancel();
            OnGotWaiver?.Invoke(this, new EventArgs());
        }

        private void Leave()
        {
            if (_token.IsCancellationRequested)
            {
                return;
            }

            OnLeave?.Invoke(this, new EventArgs());
        }

        public override string ToString() => _requestId.ToString();
    }
}

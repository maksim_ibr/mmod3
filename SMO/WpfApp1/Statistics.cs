﻿using System;
using System.Collections.Generic;
using System.Linq;
using WpfApp1.SmoElements;

namespace WpfApp1
{
    public sealed class Statistics
    {
        private readonly int _channelsCount;

        public EventList<Channel> BusyChannels;
        public EventList<string> Notifications;

        public List<Request> LeaveRequestCollection { get; }
        public List<Request> DoneRequestCollection { get; }
        public List<Request> WaiverRequestCollection { get; }
        public List<Request> AllRequestsCollection { get; }

        public EventHandler<string> OnNotify;
        public EventHandler<string> OnSateChange;

        public List<int> StateList { get; }

        public Statistics(int channelsCount, int queueCapacity)
        {
            _channelsCount = channelsCount;
            BusyChannels = new EventList<Channel>();
            LeaveRequestCollection = new List<Request>();
            DoneRequestCollection = new List<Request>();
            WaiverRequestCollection = new List<Request>();
            AllRequestsCollection = new List<Request>();
            Notifications  = new EventList<string>();
            StateList = new List<int>();
            for (var i = 0; i < channelsCount + queueCapacity + 1; i++)
            {
                StateList.Add(0);
            }

            StateList[0]++;

            BusyChannels.OnCountChanged += OnSateChanged;
            Notifications.OnCountChanged += OnNotification;
        }

        private void OnNotification(object sender, int requestCount)
        {
            if (requestCount != -1)
            {
                var index = BusyChannels.Count <= _channelsCount && requestCount < 1
                    ? BusyChannels.Count
                    : _channelsCount + requestCount;

                StateList[index]++;

                //if (index != _lastState)
                //{
                //    _lastState = index;
                //}
            }

            OnNotify?.Invoke(this, Notifications.Last());
        }

        private void OnSateChanged(object sender, int args) => OnSateChange?.Invoke(this,
            $"Состояние S{BusyChannels.Count}. Свободных: {_channelsCount - BusyChannels.Count}");
    }
}

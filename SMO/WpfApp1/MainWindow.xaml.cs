﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using WpfApp1.Assessment;
using WpfApp1.SmoElements;

namespace WpfApp1
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once RedundantExtendsListEntry
    public partial class MainWindow : Window
    {
        private static readonly object UiLock = new object();

        private const int ChannelsCount = 3;
        private const int QueueCapacity = 4;
        private const int RequestIntensity = 100;
        private const int ChannelIntensity = 3;
        private const double ExponentialParameter = 6;

        public Statistics CurrentStatistics { get; }
        private readonly Smo _smo;

        private readonly EventHandler<List<int>> _onStop;

        public MainWindow()
        {
            CurrentStatistics = new Statistics(ChannelsCount, QueueCapacity);

            InitializeComponent();

            const double expMo = ExponentialParameter;
            var finalsProbabilityT = TheoreticalProbability.CalculateFinalProbabilities(RequestIntensity, ChannelIntensity,
                ChannelsCount, QueueCapacity, 1 / expMo);
            var refusedPt = finalsProbabilityT.Item2.Last();
            var absoluteThroughputT = Assessments.CalculateAbsoluteThroughput(RequestIntensity, refusedPt);
            var averageRequestCountInSmoT =
                Assessments.CalculateAverageRequestCountInSmo(finalsProbabilityT.Item1, finalsProbabilityT.Item2);
            var averageRequestCountInQueueT = Assessments.CalculateAverageRequestCountInQueue(finalsProbabilityT.Item2);
            var averageTimeInSmoT = Assessments.CalculateAverageTimeInSmo(refusedPt, ChannelIntensity, 1 / expMo);
            var averageTimeInQueueT = Assessments.CalculateAverageTimeInQueue(averageTimeInSmoT, 1 / expMo);
            var averageBusyChannelsT = Assessments.CalculateAverageBusyChannels(absoluteThroughputT, ChannelIntensity);

            SetTheoreticalValues(finalsProbabilityT, refusedPt, absoluteThroughputT, averageRequestCountInSmoT,
                averageRequestCountInQueueT, averageTimeInSmoT, averageTimeInQueueT, averageBusyChannelsT);

            CurrentStatistics.OnSateChange += OnSateUpdated;
            CurrentStatistics.OnNotify += OnNotification;

            _smo = new Smo(CurrentStatistics, RequestIntensity, ChannelIntensity, ExponentialParameter, ChannelsCount,
                QueueCapacity);

            _onStop += OnStopped;
        }

        private void SetTheoreticalValues(Tuple<List<double>, List<double>> finalsProbabilityT, double refusedPt,
            double absoluteThroughputT, double averageRequestCountInSmoT, double averageRequestCountInQueueT,
            double averageTimeInSmoT, double averageTimeInQueueT, double averageBusyChannelsT)
        {
            lock (UiLock)
            {
                Dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    foreach (var p in finalsProbabilityT.Item1)
                    {
                        FinalPTField.Items.Add(p);
                    }

                    foreach (var p in finalsProbabilityT.Item2)
                    {
                        FinalPTField.Items.Add(p);
                    }

                    refusedPTField.Text = refusedPt.ToString(CultureInfo.InvariantCulture);
                    absoluteThroughputTField.Text = absoluteThroughputT.ToString(CultureInfo.InvariantCulture);
                    averageRequestCountInSmoTField.Text = averageRequestCountInSmoT.ToString(CultureInfo.InvariantCulture);
                    averageRequestCountInQueueTField.Text = averageRequestCountInQueueT.ToString(CultureInfo.InvariantCulture);
                    averageTimeInSmoTField.Text = averageTimeInSmoT.ToString(CultureInfo.InvariantCulture);
                    averageTimeInQueueTField.Text = averageTimeInQueueT.ToString(CultureInfo.InvariantCulture);
                    averageBusyChannelsTField.Text = averageBusyChannelsT.ToString(CultureInfo.InvariantCulture);
                }));
            }
        }

        private void SetCurrentValues(IReadOnlyCollection<double> finalsProbabilityE, double refusedPe,
            double absoluteThroughputE, double averageRequestCountInSmoE, double averageRequestCountInQueueE,
            double averageTimeInSmoE, double averageTimeInQueueE, double averageBusyChannelsE)
        {
            lock (UiLock)
            {
                Dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    foreach (var p in finalsProbabilityE)
                    {
                        FinalPEField.Items.Add(p);
                    }

                    refusedPEField.Text = refusedPe.ToString(CultureInfo.InvariantCulture);
                    absoluteThroughputEField.Text = absoluteThroughputE.ToString(CultureInfo.InvariantCulture);
                    averageRequestCountInSmoEField.Text = averageRequestCountInSmoE.ToString(CultureInfo.InvariantCulture);
                    averageRequestCountInQueueEField.Text = averageRequestCountInQueueE.ToString(CultureInfo.InvariantCulture);
                    averageTimeInSmoEField.Text = averageTimeInSmoE.ToString(CultureInfo.InvariantCulture);
                    averageTimeInQueueEField.Text = averageTimeInQueueE.ToString(CultureInfo.InvariantCulture);
                    averageBusyChannelsEField.Text = averageBusyChannelsE.ToString(CultureInfo.InvariantCulture);

                    var requestsCount = CurrentStatistics.AllRequestsCollection.Count;
                    var grValues = new ObservableCollection<KeyValuePair<int, double>>
                    {
                        new KeyValuePair<int, double>(0, (double)CurrentStatistics.WaiverRequestCollection.Count / requestsCount * 100),
                        new KeyValuePair<int, double>(1, (double)CurrentStatistics.DoneRequestCollection.Count / requestsCount * 100),
                        new KeyValuePair<int, double>(2, (double)CurrentStatistics.LeaveRequestCollection.Count / requestsCount * 100)
                    };

                    lineChart.DataContext = grValues;
                }));
            }
        }

        public void OnStopped(object sender, List<int> stateList)
        {
            var finalsProbabilityE = SmoProbability.CalculateFinalProbabilities(stateList);

            var mo = 0.0;
            for (var i = 0; i < finalsProbabilityE.Count; i++)
            {
                mo += finalsProbabilityE[i] * i;
            }

            var refusedPe = finalsProbabilityE.Last();
            var absoluteThroughputE = Assessments.CalculateAbsoluteThroughput(RequestIntensity, refusedPe);
            var finalsProbabilityEp = finalsProbabilityE.Take(ChannelsCount + 1).ToList();
            var finalsProbabilityEm = finalsProbabilityE.Skip(ChannelsCount + 1)
                .Take(finalsProbabilityE.Count - ChannelsCount - 1).ToList();
            var averageRequestCountInSmoE =
                Assessments.CalculateAverageRequestCountInSmo(finalsProbabilityEp, finalsProbabilityEm);
            var averageRequestCountInQueueE = Assessments.CalculateAverageRequestCountInQueue(finalsProbabilityEm);
            var averageTimeInSmoE = Assessments.CalculateAverageTimeInSmo(refusedPe, ChannelIntensity, 1 / mo);
            var averageTimeInQueueE = Assessments.CalculateAverageTimeInQueue(averageTimeInSmoE, 1 / mo);
            var averageBusyChannelsE = Assessments.CalculateAverageBusyChannels(absoluteThroughputE, ChannelIntensity);

            SetCurrentValues(finalsProbabilityE, refusedPe, absoluteThroughputE, averageRequestCountInSmoE,
                averageRequestCountInQueueE, averageTimeInSmoE, averageTimeInQueueE, averageBusyChannelsE);
        }

        private void OnSateUpdated(object sender, string state)
        {
            lock (UiLock)
            {
                Dispatcher.BeginInvoke(new ThreadStart(() => SmoState.Text = state));
            }
        }

        private void OnNotification(object sender, string state)
        {
            lock (UiLock)
            {
                Dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    if (Notifications.Items.Count == 0)
                    {
                        Notifications.Items.Add(state);
                    }
                    else
                    {
                        Notifications.Items.Insert(0, state);
                    }
                }));
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e) => _smo.Start();

        private void EndButton_Click(object sender, RoutedEventArgs e)
        {
            var stateList = _smo.Stop();
            _onStop?.Invoke(this, stateList);
        }
    }
}
